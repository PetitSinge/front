FROM node:14.21.3 AS build
RUN mkdir /app
COPY . /app 
WORKDIR /app
RUN npm i
RUN npm run build

FROM scratch
COPY --from=build /app/dist/* /dist

